FROM ubuntu:20.04 AS builder

WORKDIR /build

ENV PATH="${PATH}:/usr/lib/ccache" \
    DEBIAN_FRONTEND=noninteractive \
    TZ=Europe/Berlin

ARG QGIS_VERSION=3.16.1
ARG QGIS_TAG=final-3_16_1

RUN apt-get update && \
    apt-get install -y build-essential && \
    apt-get install -y \
        bison \
        ca-certificates \
        ccache \
        cmake \
        cmake-curses-gui \
        dh-python \
        doxygen \
        expect \
        flex \
        flip \
        gdal-bin \
        git \
        graphviz \
        grass-dev \
        libexiv2-dev \
        libexpat1-dev \
        libfcgi-dev \
        libgdal-dev \
        libgeos-dev \
        libgsl-dev \
        libpq-dev \
        libproj-dev \
        libprotobuf-dev \
        libqca-qt5-2-dev \
        libqca-qt5-2-plugins \
        libqscintilla2-qt5-dev \
        libqt5opengl5-dev \
        libqt5serialport5-dev \
        libqt5sql5-sqlite \
        libqt5svg5-dev \
        libqt5webkit5-dev \
        libqt5xmlpatterns5-dev \
        libqwt-qt5-dev \
        libspatialindex-dev \
        libspatialite-dev \
        libsqlite3-dev \
        libsqlite3-mod-spatialite \
        libyaml-tiny-perl \
        libzip-dev \
        lighttpd \
        locales \
        ninja-build \
        ocl-icd-opencl-dev \
        opencl-headers \
        pkg-config \
        poppler-utils \
        protobuf-compiler \
        pyqt5-dev \
        pyqt5-dev-tools \
        pyqt5.qsci-dev \
        python3-all-dev \
        python3-autopep8 \
        python3-dateutil \
        python3-dev \
        python3-future \
        python3-gdal \
        python3-httplib2 \
        python3-jinja2 \
        python3-lxml \
        python3-markupsafe \
        python3-mock \
        python3-nose2 \
        python3-owslib \
        python3-plotly \
        python3-psycopg2 \
        python3-pygments \
        python3-pyproj \
        python3-pyqt5 \
        python3-pyqt5.qsci \
        python3-pyqt5.qtsql \
        python3-pyqt5.qtsvg \
        python3-pyqt5.qtwebkit \
        python3-requests \
        python3-sip \
        python3-sip-dev \
        python3-six \
        python3-termcolor \
        python3-tz \
        python3-yaml \
        qt3d-assimpsceneimport-plugin \
        qt3d-defaultgeometryloader-plugin \
        qt3d-gltfsceneio-plugin \
        qt3d-scene2d-plugin \
        qt3d5-dev qt5-default \
        qt5keychain-dev \
        qtbase5-dev \
        qtbase5-private-dev \
        qtpositioning5-dev \
        qttools5-dev \
        qttools5-dev-tools \
        saga \
        spawn-fcgi \
        pandoc \
        xauth \
        xfonts-100dpi \
        xfonts-75dpi \
        xfonts-base \
        xfonts-scalable \
        xvfb

RUN git clone https://github.com/qgis/QGIS.git && \
    cd QGIS && \
    git checkout ${QGIS_TAG} && \
    mkdir build && \
    mkdir /build/dist

RUN cd QGIS/build && \
    cmake -G Ninja \
        -D CMAKE_INSTALL_PREFIX=/build/dist \
        -D WITH_ANALYSIS=FALSE \
        -D WITH_DESKTOP=FALSE \
        -D WITH_GRASS7=FALSE \
        -D WITH_GUI=FALSE \
        -D WITH_QGIS_PROCESS=FALSE \
        -D WITH_QT5SERIALPORT=FALSE \
        -D WITH_QTWEBKIT=FALSE \
        -D WITH_STAGED_PLUGINS=FALSE \
        -D WITH_SERVER=TRUE \
        .. && \
    ninja && \
    ninja install

FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive \
    TZ=Europe/Berlin \
    QGIS_AUTH_DB_DIR_PATH=/auth

ARG QGIS_VERSION=3.16.1

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
        fontconfig \
        spawn-fcgi \
        xvfb \
        tzdata \
        bash && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* && \
    mkdir /opt/qgis

COPY --from=builder /build/dist/bin /opt/qgis/bin/
COPY --from=builder /build/dist/include /opt/qgis/include/
COPY --from=builder /build/dist/lib /opt/qgis/lib/
COPY --from=builder /build/dist/share /opt/qgis/share/

COPY uid_entrypoint.sh /usr/local/bin/
COPY run.sh /usr/local/bin/
COPY fonts/OBJEBRG.ttf /usr/share/fonts/truetype/objebrg/

ENV LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:/opt/qgis/lib/" \
    PATH="${PATH}:/opt/qgis/bin"

RUN fc-cache -f -v && \
    adduser \
        --system \
        --uid 1001 \
        --gid 0 \
        --shell /bin/bash \
        --no-create-home \
        --disabled-password \
        --disabled-login \
        qgis && \
    mkdir -p /data && \
    chown 1001:0 /data && \
    chmod g=u /data && \
    mkdir -p /auth && \
    chown 1001:0 /auth && \
    chmod g=u /auth && \
    chgrp 0 /etc/passwd && \
    chmod g=u /etc/passwd

RUN apt-get update && \
    apt-get install -y \
        libfcgi-bin \
        libgdal26 \
        libgeos-3.8.0 \
        libgeos-c1v5 \
        libproj15 \
        libprotobuf-lite17 \
        libqca-qt5-2 \
        libqt5concurrent5 \
        libqt5core5a \
        libqt5gui5 \
        libqt5keychain1 \
        libqt5positioning5 \
        libqt5printsupport5 \
        libqt5sql5 \
        libqt5xml5 \
        libspatialindex6 \
        libzip5 \
        ocl-icd-libopencl1 && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/*

WORKDIR /data

ENTRYPOINT [ "/usr/local/bin/uid_entrypoint.sh" ]

EXPOSE 5000

CMD [ "/usr/local/bin/run.sh" ]

USER 1001
